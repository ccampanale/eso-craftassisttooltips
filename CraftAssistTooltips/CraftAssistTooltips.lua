------------------------------------------------------------------
--CraftAssistTooltips.lua
--Author: Vicster0
--v0.1.1
--[[
	Adds the option for tooltips on mouse enter for traits while crafting.
]]
------------------------------------------------------------------

CraftAssistTooltips = {}
CraftAssistTooltips.name = "CraftAssistTooltips"
CraftAssistTooltips.version = "0.1.1"
CraftAssistTooltips.settings = {}
local CraftAssistTooltips = CraftAssistTooltips

--[[--------------------------------------------------------------
--	Local Variables, Libraries, and Definitions
--]]--------------------------------------------------------------
local LAM = LibStub("LibAddonMenu-1.0");
local LAM_vicstersAddons = _vicstersAddons;

--[[--------------------------------------------------------------
--	Global Variables and external functions
--]]--------------------------------------------------------------

--ItemTraitTypes
CAT_ITEM_TRAIT_TABLE = {
	[ITEM_TRAIT_TYPE_ARMOR_DIVINES] =	[[|HFFFFFF:item:23173:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Sapphire]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_EXPLORATION]	= [[|HFFFFFF:item:23171:1:13:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|hGarnet|h]],
	[ITEM_TRAIT_TYPE_ARMOR_IMPENETRABLE]	= [[|HFFFFFF:item:23219:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Diamond]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_INFUSED]		= [[|HFFFFFF:item:30219:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Bloodstone]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_REINFORCED]	= [[|HFFFFFF:item:30221:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Sardonyx]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_STURDY]		= [[|HFFFFFF:item:4456:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Quartz]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_TRAINING]		= [[|HFFFFFF:item:4442:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Emerald]|h]],
	[ITEM_TRAIT_TYPE_ARMOR_WELL_FITTED]	= [[|HFFFFFF:item:23221:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Almandine]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_CHARGED]		= [[|HFFFFFF:item:23204:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Amethyst]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_DEFENDING]	= [[|HFFFFFF:item:813:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Turquoise]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_INFUSED]		= [[|HFFFFFF:item:810:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Jade]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_POWERED]		= [[|HFFFFFF:item:23203:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Chysolite]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_PRECISE]		= [[|HFFFFFF:item:4486:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Ruby]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_SHARPENED]	= [[|HFFFFFF:item:23149:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Fire Opal]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_TRAINING]		= [[|HFFFFFF:item:23165:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Carnelian]|h]],
	[ITEM_TRAIT_TYPE_WEAPON_WEIGHTED]		= [[|HFFFFFF:item:16291:0:1:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0|h[Citrine]|h]],
}

CAT_CRAFTING_TRAIT_LIST_ITEMS_ORIGINAL_MOUSE_HANDLERS = {}

function CraftAssistTooltips.CAT_NewTraitListItemMouseEnterHandler( control, ... )
	if( control.traitType ~= nil and control.traitType ~= ITEM_TRAIT_TYPE_NONE) then
		--[[ Tie In for InventoryInsight -- Update the global link IN2_CURRENT_MOUSEOVER_lIN ]]
		IN2_CURRENT_MOUSEOVER_LINK = CAT_ITEM_TRAIT_TABLE[control.traitType]
		---------------------------------------------------------------------------------------
		InitializeTooltip(ItemTooltip, ZO_SmithingTopLevelCreationPanelResultTooltip, TOP, 0, -800, 0)
		ItemTooltip:SetHidden(false)
		ItemTooltip:ClearLines()
		ItemTooltip:SetLink(CAT_ITEM_TRAIT_TABLE[control.traitType])
	end
end

function CraftAssistTooltips.CAT_NewTraitListItemMouseExitHandler( control, ... )
	ItemTooltip:SetHidden(true)
end

--[[--------------------------------------------------------------
--	CraftAssistTooltips :CraftAssistTooltips.CAT_DebugOutput
		Receives: output
			Displays output to chatframe using d() only if debug is enabled.
--]]--------------------------------------------------------------
function CraftAssistTooltips.CAT_DebugOutput( output )
	if(CraftAssistTooltips.settings.caDebug) then
		d(output);
	end
end

function CraftAssistTooltips.CAT_TraitListReplaceMouseEnterOnChildren( setCAHandlers )
	local numc = ZO_SmithingTopLevelCreationPanelTraitListListScroll:GetNumChildren()
	for i=1, numc do
		local traitItem = ZO_SmithingTopLevelCreationPanelTraitListListScroll:GetChild(i);
		--local tempTraitMouseEnterHandler = traitItem:GetHandler("OnMouseEnter");			
		--local tempTraitMouseExitHandler = traitItem:GetHandler("OnMouseExit");
		if( setCAHandlers ) then
			traitItem:SetHandler("OnMouseEnter", CraftAssistTooltips.CAT_NewTraitListItemMouseEnterHandler)
			traitItem:SetHandler("OnMouseExit", CraftAssistTooltips.CAT_NewTraitListItemMouseExitHandler)
		else
			traitItem:SetHandler("OnMouseEnter", nil)
			traitItem:SetHandler("OnMouseExit", nil)
		end
	end
end

function CraftAssistTooltips.CAT_CraftingFrameOnShowCallback( ... )
	if ( CraftAssistTooltips.settings.caToggle ) then
		if( ZO_SmithingTopLevel.CAT_HasModified == nil or ZO_SmithingTopLevel.CAT_HasModified == false ) then
			CraftAssistTooltips.CAT_DebugOutput("[CraftAssistTooltips]:Enabling CAT tooltips...")
			--ZO_SmithingTopLevelCreationPanelTraitListListScroll
			CraftAssistTooltips.CAT_TraitListReplaceMouseEnterOnChildren(true)
			--ZO_SmithingTopLevelCreationPanelStyleListListScroll 		--possibly in a future version
			--ZO_SmithingTopLevelCreationPanelMaterialListListScroll	--possibly in a future version
			ZO_SmithingTopLevel.CAT_HasModified = true 
		else
			ZO_SmithingTopLevel.CAT_HasModified = false		
		end
	else
		if( ZO_SmithingTopLevel.CAT_HasModified == true ) then
			CraftAssistTooltips.CAT_DebugOutput("[CraftAssistTooltips]:Disabling CAT tooltips...")
			--ZO_SmithingTopLevelCreationPanelTraitListListScroll
			CraftAssistTooltips.CAT_TraitListReplaceMouseEnterOnChildren(false)
			--ZO_SmithingTopLevelCreationPanelStyleListListScroll 		--possibly in a future version
			--ZO_SmithingTopLevelCreationPanelMaterialListListScroll	--possibly in a future version
			ZO_SmithingTopLevel.CAT_HasModified = false 
		else
			--ZO_SmithingTopLevel.CAT_HasModified = false		
		end
	end
end

function CraftAssistTooltips.CAT_SetupControlEventCallbacks( ... )
	EVENT_MANAGER:RegisterForEvent("CAT_SETUPCRAFT", EVENT_CRAFTING_STATION_INTERACT, CraftAssistTooltips.CAT_CraftingFrameOnShowCallback)
end

--[[--------------------------------------------------------------
--	CraftAssistTooltips:CraftAssistTooltips.CAT_Loaded
		Receives: eventCode, addOnName
			CallBack for AddOn Initializeation - confirms addOnName, sets default settings, 
			loads saves settings, registers SLASH_COMMANDS to handler callBack function, registers
			for events, and sets callback Handlers for ToolTips.
--]]--------------------------------------------------------------
function CraftAssistTooltips.CAT_Loaded(eventCode, addOnName)
	if(addOnName ~= "CraftAssistTooltips") then
        return
    end	
	
	local defaultSettings = {
		caToggle = true;
		caDebug = false;
	}

	CraftAssistTooltips.settings = ZO_SavedVars:New("CraftAssistTooltips_Settings", 1, nil, defaultSettings)
	
    SLASH_COMMANDS["/CraftAssistTooltips"] = CraftAssistTooltips.CAT_SlashCommands
	SLASH_COMMANDS["/cat"] = SLASH_COMMANDS["/CraftAssistTooltips"]
	
	CraftAssistTooltips.CAT_CreateSettingsWindow()
	
	CraftAssistTooltips.CAT_SetupControlEventCallbacks()
end

--[[--------------------------------------------------------------
--	CraftAssistTooltips :CraftAssistTooltips.CAT_SlashCommands
		Receives: cmd
			Primary function to handle all SlashCommands for ca. 
			Processes [cmd] and executes appropriate commands or local settings update.
	
--]]--------------------------------------------------------------
function CraftAssistTooltips.CAT_SlashCommands( cmd )
	
	if ( cmd == "" ) then
    	d("[CraftAssistTooltips]:Usage - ")
    	d("	/cat or /CraftAssistTooltips [options]")
    	d(" 	")
    	d("	Options")
    	d("		on - Enables the CraftAssistTooltips addon.")
    	d("		off - Disables the CraftAssistTooltips addon.")
    	d("		debug - Enables debug functionality for the CraftAssistTooltips addon.")
		return
	end
	
	if ( cmd == "on" ) then
    	if CraftAssistTooltips.settings.caToggle then
    		d("[CraftAssistTooltips]:Addon already enabled...")
    	else
		   	CraftAssistTooltips.settings.caToggle = true;
    		d("[CraftAssistTooltips]:Addon enabled.")
    	end
		return
	end
	
	if ( cmd == "off" ) then
    	if not CraftAssistTooltips.settings.caToggle then
    		d("[CraftAssistTooltips]:Addon already disable...")
    	else
    		d("[CraftAssistTooltips]:Addon disabled.")
			CraftAssistTooltips.settings.caToggle = false;
    	end		
		return
	end

	if ( cmd == "debug" ) then
		if( CraftAssistTooltips.settings.caDebug ) then
			d("[CraftAssistTooltips]:Debug[Off]")
			CraftAssistTooltips.settings.caDebug = false;
		else
			d("[CraftAssistTooltips]:Debug[On]")
			CraftAssistTooltips.settings.caDebug = true;			
		end
		return
	end
end

--[[--------------------------------------------------------------
--	CraftAssistTooltips:CraftAssistTooltips.CAT_CreateSettingsWindow
		Uses LAM Library to create a settings windows for CraftAssistTooltips.
--]]--------------------------------------------------------------
function CraftAssistTooltips.CAT_CreateSettingsWindow()

	if( LAM_vicstersAddons == nil) then
		LAM_vicstersAddons = LAM:CreateControlPanel("_vicstersAddons", "Vicster's Addons")
	end

	LAM:AddHeader(LAM_vicstersAddons, "CAGeneral", "Craft Assist")
	
	LAM:AddCheckbox(LAM_vicstersAddons, "caActiveTooltips", 'Trait Item ToolTips', 'Enables/Disables the showing the tooltips for trait items while crafting.',
    function()
      return CraftAssistTooltips.settings.caToggle;
    end,
    function()
        if CraftAssistTooltips.settings.caToggle then
    		CraftAssistTooltips.settings.caToggle = false;
    		d("[CraftAssistTooltips]:[Off].")
    	else
		   	CraftAssistTooltips.settings.caToggle = true;
    		d("[CraftAssistTooltips]:[On].")
    	end
    end
	)

	LAM:AddCheckbox(LAM_vicstersAddons, "caActiveDebugging", 'Debugging', 'Prints verbose debugging to the ChatFrame.',
    function()
      return CraftAssistTooltips.settings.caDebug;
    end,
    function()
		if( CraftAssistTooltips.settings.caDebug ) then
			d("[CraftAssistTooltips]:Debug[Off]")
			CraftAssistTooltips.settings.caDebug = false;
		else
			d("[CraftAssistTooltips]:Debug[On]")
			CraftAssistTooltips.settings.caDebug = true;			
		end
		return
    end
	)
end

--[[--------------------------------------------------------------
--	CraftAssistTooltips:CraftAssistTooltips_Initialized
		Initializes the addon and registers for the EVENT_ADD_ON_LOADED with a callback to 
		the local function CraftAssistTooltips_Loaded().
--]]--------------------------------------------------------------
function CraftAssistTooltips.CAT_Initialized()
	EVENT_MANAGER:RegisterForEvent("CraftAssistTooltipsLoaded", EVENT_ADD_ON_LOADED, CraftAssistTooltips.CAT_Loaded)
end


CraftAssistTooltips.CAT_Initialized()




