## Title: Craft Assist Tooltips
## Author: vicster0
## Version: 0.1.1
## APIVersion: 100004
## SavedVariables: CraftAssistTooltips_Settings
## OptionalDependsOn: LibAddonMenu-1.0

libs\LibStub\LibStub.lua
libs\LibAddonMenu-1.0\LibAddonMenu-1.0.lua

CraftAssistTooltips.lua
CraftAssistTooltips.xml